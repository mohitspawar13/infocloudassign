import 'dart:math';

import 'package:firebase_application/view/user/loginpage.dart';
import 'package:firebase_application/view/util/utils.dart';
import 'package:firebase_application/view/user/validatenumberpage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class SignUPPage extends StatefulWidget {
  const SignUPPage({super.key});

  @override
  State<SignUPPage> createState() => _SignUPPageState();
}

class _SignUPPageState extends State<SignUPPage> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();

  bool varified = false;

  final databaseref = FirebaseDatabase.instance.ref('userDetails');
  // TextEditingController nameController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.purpleAccent,
        title: const Text(
          "SignupPage",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          const SizedBox(
            height: 30,
          ),
          Form(
            key: formkey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(
                        hintText: "Customer Name",
                        prefixIcon: const Icon(Icons.person_add_alt_outlined),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter Name";
                      } else {
                        return null;
                      }
                      // if (value!.isEmpty) {
                      //   const SnackBar(
                      //     content: Text("Enter name"),
                      //   );
                      // }
                      // return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: numberController,
                    decoration: InputDecoration(
                        hintText: "Contanct Number",
                        prefixIcon: const Icon(Icons.contact_phone),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        const SnackBar(
                          content: Text("Enter Number"),
                        );
                        if (value.isEmpty) {
                          return "Enter Contact No";
                        } else {
                          return null;
                        }
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                        hintText: "Email",
                        prefixIcon: const Icon(Icons.email_outlined),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter Email";
                      } else {
                        return null;
                      }
                      // if (value!.isEmpty) {
                      //   const SnackBar(
                      //     content: Text("Enter email"),
                      //   );
                      // }
                      // return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: addressController,
                    decoration: InputDecoration(
                        hintText: "Address",
                        prefixIcon: const Icon(Icons.home),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter Address";
                      } else {
                        return null;
                      }
                      // if (value!.isEmpty) {
                      //   const SnackBar(
                      //     content: Text("Enter address"),
                      //   );
                      //  }
                      // return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: cityController,
                    decoration: InputDecoration(
                        hintText: "City",
                        prefixIcon: const Icon(Icons.location_city),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter City";
                      } else {
                        return null;
                      }
                      // if (value!.isEmpty) {
                      //   const SnackBar(
                      //     content: Text("Enter city"),
                      //   );
                      // }
                      // return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: pincodeController,
                    decoration: InputDecoration(
                        hintText: "Pincode",
                        prefixIcon: const Icon(Icons.location_pin),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter PinCode";
                      } else {
                        return null;
                      }
                      // if (value!.isEmpty) {
                      //   const SnackBar(
                      //     content: Text("Enter pincode"),
                      //   );
                      // }
                      // return null;
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: passwordController,
                    decoration: InputDecoration(
                        hintText: "Password",
                        prefixIcon: const Icon(Icons.password_sharp),
                        contentPadding: const EdgeInsets.all(15),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter password";
                      } else {
                        return null;
                      }
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      _auth.verifyPhoneNumber(
                          phoneNumber: numberController.text.toString(),
                          verificationCompleted: (_) {},
                          verificationFailed: (e) {
                            Utils().toastmessage(e.toString());
                          },
                          codeSent: (String varification, int? token) {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ValidateNumber(
                                varificationId: varification,
                              );
                            }));
                          },
                          codeAutoRetrievalTimeout: (e) {
                            Utils().toastmessage(e.toString());
                          });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.purpleAccent,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      width: 300,
                      height: 50,
                      child: const Text(
                        'Verifying Number',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 16),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      if (formkey.currentState!.validate()) {
                        _auth
                            .createUserWithEmailAndPassword(
                                email: emailController.text.toString(),
                                password: passwordController.text.toString())
                            .then((value) {
                          // Utils().toastmessage("varify your mobile number");
                        }).onError((error, stackTrace) {
                          Utils().toastmessage(error.toString());
                          log(error.toString() as num);
                        });
                      }
                      databaseref.child(index.toString()).set({
                        'name': nameController.text.toString(),
                        'email': emailController.text.toString(),
                        'number': numberController.text.toString(),
                        'address': addressController.text.toString(),
                        'city': cityController.text.toString(),
                        'pincode': pincodeController.text.toString(),
                        'password': passwordController.text.toString()
                      }).then((value) {
                        Utils().toastmessage("succesful entry");
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return const LoginPage();
                        }));
                      }).onError((error, stackTrace) {
                        Utils().toastmessage(error.toString());
                      });
                      index++;
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.purpleAccent,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      width: 300,
                      height: 50,
                      child: const Text(
                        'SignUp',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ]),
      ),
    );
  }
}
